PREFIX ?= /usr
ALPMDIR = $(PREFIX)/share/libalpm
SCRIPTSDIR = $(ALPMDIR)/scripts
HOOKSDIR = $(ALPMDIR)/hooks
BINDIR = $(PREFIX)/bin

OPENRCSCRIPTS = $(wildcard openrc/scripts/*)
OPENRCHOOKS = $(wildcard openrc/hooks/*)

OPENRCCRONIEHOOKS = $(wildcard openrc/cronie-hooks/*)
OPENRCDBUSHOOKS = $(wildcard openrc/dbus-hooks/*)
OPENRCATDHOOKS = $(wildcard openrc/atd-hooks/*)

RUNITSCRIPTS = $(wildcard runit/scripts/*)
RUNITHOOKS = $(wildcard runit/hooks/*)

S6SCRIPTS = $(wildcard s6/scripts/*)
S6HOOKS = $(wildcard s6/hooks/*)

S6CRONIEHOOKS = $(wildcard s6/cronie-hooks/*)
S6DBUSHOOKS = $(wildcard s6/dbus-hooks/*)
S6ATDHOOKS = $(wildcard s6/atd-hooks/*)

DINITSCRIPTS = $(wildcard dinit/scripts/*)
DINITHOOKS = $(wildcard dinit/hooks/*)

DINITCRONIEHOOKS = $(wildcard dinit/cronie-hooks/*)
DINITDBUSHOOKS = $(wildcard dinit/dbus-hooks/*)
DINITATDHOOKS = $(wildcard dinit/atd-hooks/*)

UDEVSCRIPTS = $(wildcard udev/scripts/*)
UDEVHOOKS = $(wildcard udev/hooks/*)

BASESCRIPTS = $(wildcard base/scripts/*)
BASEHOOKS = $(wildcard base/hooks/*)

TMPFILESHOOKS = $(wildcard etmpfiles/hooks/*)

SYSUSERSHOOKS = $(wildcard esysusers/hooks/*)

WRAPPER = $(wildcard wrapper/*)

DMODE = -dm0755
MODE = -m0644
EMODE = -m0755

install_common:
	install $(DMODE) $(DESTDIR)$(SCRIPTSDIR)
	install $(DMODE) $(DESTDIR)$(HOOKSDIR)

install_hook_common:
	install $(DMODE) $(DESTDIR)$(HOOKSDIR)

install_base: install_common
	install $(EMODE) $(BASESCRIPTS) $(DESTDIR)$(SCRIPTSDIR)
	install $(MODE) $(BASEHOOKS) $(DESTDIR)$(HOOKSDIR)

install_udev: install_common
	install $(EMODE) $(UDEVSCRIPTS) $(DESTDIR)$(SCRIPTSDIR)
	install $(MODE) $(UDEVHOOKS) $(DESTDIR)$(HOOKSDIR)

install_tmpfiles: install_hook_common
	install $(MODE) $(TMPFILESHOOKS) $(DESTDIR)$(HOOKSDIR)

install_sysusers: install_hook_common
	install $(MODE) $(SYSUSERSHOOKS) $(DESTDIR)$(HOOKSDIR)

install_openrc: install_common
	install $(EMODE) $(OPENRCSCRIPTS) $(DESTDIR)$(SCRIPTSDIR)
	install $(MODE) $(OPENRCHOOKS) $(DESTDIR)$(HOOKSDIR)

install_runit: install_common
	install $(EMODE) $(RUNITSCRIPTS) $(DESTDIR)$(SCRIPTSDIR)
	install $(MODE) $(RUNITHOOKS) $(DESTDIR)$(HOOKSDIR)

install_s6: install_common
	install $(EMODE) $(S6SCRIPTS) $(DESTDIR)$(SCRIPTSDIR)
	install $(MODE) $(S6HOOKS) $(DESTDIR)$(HOOKSDIR)

install_dinit: install_common
	install $(EMODE) $(DINITSCRIPTS) $(DESTDIR)$(SCRIPTSDIR)
	install $(MODE) $(DINITHOOKS) $(DESTDIR)$(HOOKSDIR)

install_openrc_cronie: install_hook_common
	install $(MODE) $(OPENRCCRONIEHOOKS) $(DESTDIR)$(HOOKSDIR)

install_openrc_at: install_hook_common
	install $(MODE) $(OPENRCATDHOOKS) $(DESTDIR)$(HOOKSDIR)

install_openrc_dbus: install_hook_common
	install $(MODE) $(OPENRCDBUSHOOKS) $(DESTDIR)$(HOOKSDIR)

install_s6_cronie: install_hook_common
	install $(MODE) $(S6CRONIEHOOKS) $(DESTDIR)$(HOOKSDIR)

install_s6_at: install_hook_common
	install $(MODE) $(S6ATDHOOKS) $(DESTDIR)$(HOOKSDIR)

install_s6_dbus: install_hook_common
	install $(MODE) $(S6DBUSHOOKS) $(DESTDIR)$(HOOKSDIR)

install_dinit_cronie: install_hook_common
	install $(MODE) $(DINITCRONIEHOOKS) $(DESTDIR)$(HOOKSDIR)

install_dinit_at: install_hook_common
	install $(MODE) $(DINITATDHOOKS) $(DESTDIR)$(HOOKSDIR)

install_dinit_dbus: install_hook_common
	install $(MODE) $(DINITDBUSHOOKS) $(DESTDIR)$(HOOKSDIR)

install_wrapper:
	install $(DMODE) $(DESTDIR)$(BINDIR)
	install $(EMODE) $(WRAPPER) $(DESTDIR)$(BINDIR)

.PHONY: install install_base install_s6 install_openrc install_runit install_dinit install_wrapper
